package com.example.remindmelatermvp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.remindmelatermvp.mvp.entities.Reminder;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    static final String DATABASE_NAME = "reminder_database";
    static final int DATABASE_VERSION = 1;

    private static DatabaseHelper mInstance = null;

    public static final String REMINDER_TABLE = "reminder_table";
    public static final String REMINDER_ID = "reminder_id";
    public static final String REMINDER_TITLE = "reminder_text";
    public static final String REMINDER_DESCRIPTION = "reminder_description";
    public static final String REMINDER_IS_ACTIVE = "reminder_is_active";
    public static final String REMINDER_DATE = "reminder_date";
    public static final String REMINDER_REPEAT_TYPE = "reminder_repeat_type";
    public static final String REMINDER_INTERVAL = "reminder_interval";
    public static final String REMINDER_TIMES_OCCURRED = "reminder_time_occurred";
    public static final String REMINDER_TIMES_OCCUR = "reminder_time_occur";
    public static final String REMINDER_IS_FOREVER = "reminder_is_forever";


    public static DatabaseHelper getInstance(Context mContext) {
        if (mInstance == null) {
            mInstance = new DatabaseHelper(mContext.getApplicationContext());
        }
        return mInstance;
    }

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + REMINDER_TABLE + " ("
                + REMINDER_ID + " INTEGER PRIMARY KEY, "
                + REMINDER_TITLE + " TEXT, "
                + REMINDER_DESCRIPTION + " TEXT,"
                + REMINDER_IS_ACTIVE + " INTEGER, "
                + REMINDER_DATE + " TEXT, "
                + REMINDER_REPEAT_TYPE + " INTEGER, "
                + REMINDER_INTERVAL + " INTEGER, "
                + REMINDER_TIMES_OCCUR + " INTEGER, "
                + REMINDER_TIMES_OCCURRED + " INTEGER, "
                + REMINDER_IS_FOREVER + " TEXT "
                + " )";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public List<Reminder> getAllReminder(int active) {
        List<Reminder> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query;
        switch (active) {
            default:
                query = "SELECT * FROM " + REMINDER_TABLE + " WHERE " + REMINDER_IS_ACTIVE + " = 1 " + " ORDER BY " + REMINDER_DATE;
                break;
            case 0:
                query = "SELECT * FROM " + REMINDER_TABLE + " WHERE " + REMINDER_IS_ACTIVE + " = 0 " + " ORDER BY " + REMINDER_DATE + " DESC";
                break;
        }

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Reminder reminder = new Reminder();

                reminder.setId(cursor.getInt(0));
                reminder.setTitle(cursor.getString(1));
                reminder.setDescription(cursor.getString(2));
                reminder.setIsActive(cursor.getInt(3));
                reminder.setDateAndTime(cursor.getLong(4));
                reminder.setRepeatType(cursor.getInt(5));
                reminder.setInterval(cursor.getInt(6));
                reminder.setTimeOccur(cursor.getInt(7));
                reminder.setTimeOccurred(cursor.getInt(8));
                reminder.setIsForever(cursor.getString(9));

                list.add(reminder);
                cursor.moveToNext();
            }
        }
        return list;
    }

    public void deleteReminder(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(REMINDER_TABLE, REMINDER_ID + " = ?", new String[]{String.valueOf(id)});
        db.close();
    }


    public int getLastID() {
        int data = 0;
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT " + REMINDER_ID + " FROM " + REMINDER_TABLE + " ORDER BY " + REMINDER_ID + " DESC LIMIT 1", null);
        if (cursor != null && cursor.moveToFirst()) {
            data = cursor.getInt(0);
            cursor.close();
        }
        return data;
    }

    public void addReminder(Reminder reminder) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(REMINDER_ID, reminder.getId());
        values.put(REMINDER_TITLE, reminder.getTitle());
        values.put(REMINDER_DESCRIPTION, reminder.getDescription());
        values.put(REMINDER_DATE, reminder.getDateAndTime());
        values.put(REMINDER_IS_ACTIVE, reminder.getIsActive());
        values.put(REMINDER_REPEAT_TYPE, reminder.getRepeatType());
        values.put(REMINDER_INTERVAL, reminder.getInterval());
        values.put(REMINDER_TIMES_OCCUR, reminder.getTimeOccur());
        values.put(REMINDER_TIMES_OCCURRED, reminder.getTimeOccurred());
        values.put(REMINDER_IS_FOREVER, reminder.getIsForever());


        database.replace(REMINDER_TABLE, null, values);

        database.close();
    }

    public Reminder getReminder(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(REMINDER_TABLE, new String[]{
                REMINDER_ID,
                REMINDER_TITLE,
                REMINDER_DESCRIPTION,
                REMINDER_IS_ACTIVE,
                REMINDER_DATE,
                REMINDER_REPEAT_TYPE,
                REMINDER_INTERVAL,
                REMINDER_TIMES_OCCUR,
                REMINDER_TIMES_OCCURRED,
                REMINDER_IS_FOREVER

        }, REMINDER_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        Reminder reminder = new Reminder(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                cursor.getInt(3), cursor.getLong(4), cursor.getInt(5), cursor.getInt(6), cursor.getInt(7),cursor.getInt(8), cursor.getString(9));
        return reminder;
    }

}
