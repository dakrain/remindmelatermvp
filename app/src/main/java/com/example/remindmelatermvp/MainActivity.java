package com.example.remindmelatermvp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.remindmelatermvp.mvp.entities.Reminder;
import com.example.remindmelatermvp.mvp.features.mainpage.MainTaskActivity;
import com.example.remindmelatermvp.utils.DateAndTimeUtil;
import com.example.remindmelatermvp.utils.NotificationUtil;
import com.maltaisn.recurpicker.Recurrence;
import com.maltaisn.recurpicker.RecurrenceFormat;
import com.maltaisn.recurpicker.RecurrencePickerDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements RecurrencePickerDialog.RecurrenceSelectedCallback  {
    private Recurrence recurrence;
    private RecurrenceFormat formatter;
    private DateFormat dateFormatShort;
    private RecurrencePickerDialog picker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//        Calendar calendar = Calendar.getInstance();
//        Date date = calendar.getTime();
//        Log.e("DEBUG", "GET " + date);
//        Log.e("DEBUG", "UTIL " + DateAndTimeUtil.parseDateAndTime(date.getTime()));
//        Log.e("DEBUG", "LONG " + date.getTime());

        // Create date formats
        final DateFormat dateFormatLong = new SimpleDateFormat("EEE MMM dd, yyyy", Locale.ENGLISH);  // Sun Dec 31, 2017
        dateFormatShort = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);  // 31-12-2017

        // Create the recurrence formatter, used to create a localized recurrence text.
        formatter = new RecurrenceFormat(this, dateFormatLong);

        picker = new RecurrencePickerDialog();
        picker.setDateFormat(dateFormatShort, dateFormatLong);
    }

    @OnClick(R.id.debugBtn)
    public void debugOnClick() {
        // Open recurrence picker dialog
        picker.setRecurrence(recurrence, System.currentTimeMillis());
        picker.show(getSupportFragmentManager(), "recur_picker");
    }

    @OnClick(R.id.mainBtn)
    public void mainOnClick() {
        MainTaskActivity.showMe(this);
    }

    @Override
    public void onRecurrencePickerSelected(Recurrence r) {
        // Called when recurrence picker dialog creates a recurrence
        recurrence = r;
        Toast.makeText(this, "Recurrence selected: " +
                formatter.format(recurrence), Toast.LENGTH_LONG).show();

    }

    @Override
    public void onRecurrencePickerCancelled(Recurrence r) {
        // Called when cancel button is clicked (if shown) or if dialog is cancelled by click outside
        Toast.makeText(this, "Picker dialog cancelled", Toast.LENGTH_SHORT).show();
    }
}

