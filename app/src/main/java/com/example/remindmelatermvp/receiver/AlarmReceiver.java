package com.example.remindmelatermvp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.remindmelatermvp.database.DatabaseHelper;
import com.example.remindmelatermvp.mvp.entities.Reminder;
import com.example.remindmelatermvp.utils.AlarmUtil;
import com.example.remindmelatermvp.utils.NotificationUtil;

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(context);
        Reminder reminder = databaseHelper.getReminder(intent.getIntExtra("REMINDER_ID", 0));
        reminder.setTimeOccurred(reminder.getTimeOccurred() + 1);
        if (reminder.getTimeOccurred() + 1 > reminder.getTimeOccur()) {
            reminder.setIsActive(0);
            Log.e("DEBUG", "ALARM SẼ TẮT");
        }

        databaseHelper.addReminder(reminder);
        NotificationUtil.createNotification(context, reminder);

        if (reminder.getTimeOccur() > reminder.getTimeOccurred() || Boolean.parseBoolean(reminder.getIsForever())) {
            AlarmUtil.setScheduleAlarm(context, reminder);
            Log.e("DEBUG", "Nhận lệnh set alarm tiếp theo");
        }

        // Dùng để gửi broadcast update lại list khi 1 alarm xảy ra
        Intent updateIntent = new Intent("BROADCAST_REFRESH");
        LocalBroadcastManager.getInstance(context).sendBroadcast(updateIntent);
        databaseHelper.close();
    }
}
