package com.example.remindmelatermvp.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.example.remindmelatermvp.mvp.entities.Reminder;
import com.example.remindmelatermvp.receiver.AlarmReceiver;

import java.util.Calendar;
import java.util.Date;

public class AlarmUtil {

    public static void setAlarm(Context context, Intent intent, int remimderID, Calendar calendar) {
        intent.putExtra("REMINDER_ID", remimderID);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, remimderID, intent, PendingIntent.FLAG_UPDATE_CURRENT);


        long currentTime = calendar.getTimeInMillis();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, currentTime, pendingIntent);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, currentTime, pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, currentTime, pendingIntent);
        }
    }

    public static void cancelAlarm(Context context, Intent intent, int notificationId) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(pendingIntent);
    }

    public static void setScheduleAlarm(Context context, Reminder reminder) {
        Calendar calendar = DateAndTimeUtil.parseLongToCalendar(reminder.getDateAndTime());
        calendar.set(Calendar.SECOND, 0);

        switch (reminder.getRepeatType()) {
            case Reminder.HOURLY:
                calendar.add(Calendar.HOUR, reminder.getInterval());
                break;
            case Reminder.DAILY:
                calendar.add(Calendar.DATE, reminder.getInterval());
                break;
            case Reminder.WEEKLY:
                calendar.add(Calendar.WEEK_OF_YEAR, reminder.getInterval());
                break;
            case Reminder.MONTHLY:
                calendar.add(Calendar.MONTH, reminder.getInterval());
                break;
            case Reminder.YEARLY:
                calendar.add(Calendar.YEAR, reminder.getInterval());
                break;
        }

        Intent intent = new Intent(context, AlarmReceiver.class);
        Log.e("DEBUG", "Calendar tiếp theo : " + DateAndTimeUtil.toStringDateAndTime(calendar));
        setAlarm(context, intent, reminder.getId(), calendar);

    }
}
