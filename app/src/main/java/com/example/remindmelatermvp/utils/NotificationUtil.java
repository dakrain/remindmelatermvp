package com.example.remindmelatermvp.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.example.remindmelatermvp.MainActivity;
import com.example.remindmelatermvp.R;
import com.example.remindmelatermvp.mvp.entities.Reminder;
import com.example.remindmelatermvp.mvp.features.viewtask.ViewTaskActivity;

public class NotificationUtil {

    public static void createNotification(Context context, Reminder reminder) {
        Intent intent = new Intent(context, ViewTaskActivity.class);
        intent.putExtra("REMINDER_ID", reminder.getId());

        PendingIntent pending = PendingIntent.getActivity(context, reminder.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("Đây là ID Channel của Khoa",
                    "Remind_Me_Later",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("Nhắc việc");
            mNotificationManager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "Đây là ID Channel của Khoa")
                .setSmallIcon(R.drawable.ic_accessibility_white_24dp)
                .setContentTitle(reminder.getTitle())
                .setContentText(reminder.getDescription())
                .setTicker(reminder.getTitle())
                .setContentIntent(pending);

        builder.setSound(Uri.parse("content://settings/system/notification_sound"));


        mNotificationManager.notify(reminder.getId(), builder.build());
    }

    public static void cancelNotification(Context context, int notificationId) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(notificationId);
    }
}
