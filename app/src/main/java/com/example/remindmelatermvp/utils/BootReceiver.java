package com.example.remindmelatermvp.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.remindmelatermvp.database.DatabaseHelper;
import com.example.remindmelatermvp.mvp.entities.Reminder;
import com.example.remindmelatermvp.receiver.AlarmReceiver;

import java.util.Calendar;
import java.util.List;

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        List<Reminder> reminderList = DatabaseHelper.getInstance(context).getAllReminder(1);
        DatabaseHelper.getInstance(context).close();
        Intent alarmIntent = new Intent(context, AlarmReceiver.class);

        for (Reminder reminder : reminderList) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(reminder.getDateAndTime());
            calendar.set(Calendar.SECOND, 0);

            AlarmUtil.setAlarm(context, alarmIntent, reminder.getId(), calendar);
        }

    }
}
