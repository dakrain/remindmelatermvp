package com.example.remindmelatermvp.mvp.entities;

public class Reminder {

    // Reminder types

    private int id;
    private int mActive;
    private int mInterval;
    private int mTimeOccur;
    private int mTimeOccurred;
    private int mRepeatType;

    public static final int DOES_NOT_REPEAT = 0;
    public static final int HOURLY = 1;
    public static final int DAILY = 2;
    public static final int WEEKLY = 3;
    public static final int MONTHLY = 4;
    public static final int YEARLY = 5;


    private String mTitle, mDescription, isForever;
    private long mDateAndTime;


    public Reminder() {
    }

    public Reminder(int id, String mTitle, String mDescription, int mActive, long mDateAndTime, int mRepeatType, int mInterval, int mTimeOccur, int mTimeOccurred, String isForever) {
        this.id = id;
        this.mTitle = mTitle;
        this.mDescription = mDescription;
        this.mActive = mActive;
        this.mDateAndTime = mDateAndTime;
        this.mRepeatType = mRepeatType;
        this.mInterval = mInterval;
        this.mTimeOccurred = mTimeOccurred;
        this.mTimeOccur = mTimeOccur;
        this.isForever = isForever;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public long getDateAndTime() {
        return mDateAndTime;
    }

    public void setDateAndTime(long mDateAndTime) {
        this.mDateAndTime = mDateAndTime;
    }


    public int getIsActive() {
        return mActive;
    }

    public void setIsActive(int active) {
        mActive = active;
    }


    public int getInterval() {
        return mInterval;
    }

    public void setInterval(int mInterval) {
        this.mInterval = mInterval;
    }

    public int getTimeOccurred() {
        return mTimeOccurred;
    }

    public void setTimeOccurred(int mTimeOccurred) {
        this.mTimeOccurred = mTimeOccurred;
    }

    public int getRepeatType() {
        return mRepeatType;
    }

    public void setRepeatType(int mRepeatType) {
        this.mRepeatType = mRepeatType;
    }

    public String getIsForever() {
        return isForever;
    }

    public void setIsForever(String isForever) {
        this.isForever = isForever;
    }

    public int getTimeOccur() {
        return mTimeOccur;
    }

    public void setTimeOccur(int mTimeOccur) {
        this.mTimeOccur = mTimeOccur;
    }
}
