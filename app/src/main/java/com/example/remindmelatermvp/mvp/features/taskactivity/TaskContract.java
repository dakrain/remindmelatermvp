package com.example.remindmelatermvp.mvp.features.taskactivity;

import android.content.Context;

import com.example.remindmelatermvp.mvp.entities.Reminder;

public interface TaskContract {
    public interface View {
        Context getContext();

        void showSucces(boolean isSuccess);

        void showEditReminder(Reminder reminder);

        void showTime(String time);

        void showDate(String date);

    }

    public interface Presenter {
        void addReminder(Reminder reminder);

        void caculatedDate(long date);

        void caculatedTime(long time);

        void getReminder(int id);

        void initView();
    }
}
