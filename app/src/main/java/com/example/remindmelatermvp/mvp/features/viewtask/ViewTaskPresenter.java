package com.example.remindmelatermvp.mvp.features.viewtask;

import android.content.Context;

import com.example.remindmelatermvp.R;
import com.example.remindmelatermvp.database.DatabaseHelper;
import com.example.remindmelatermvp.mvp.entities.Reminder;

public class ViewTaskPresenter implements ViewTaskContract.Presenter {

    private ViewTaskContract.View mvpView;
    private Context context;
    private DatabaseHelper databaseHelper;

    public ViewTaskPresenter(ViewTaskContract.View mvpView, Context context) {
        this.context = context;
        this.mvpView = mvpView;
    }

    @Override
    public void deleteReminder(int id) {
        databaseHelper = DatabaseHelper.getInstance(context);
        databaseHelper.deleteReminder(id);
        databaseHelper.close();

        mvpView.showSucces(true);
    }

    @Override
    public void getReminder(int id) {
        databaseHelper = DatabaseHelper.getInstance(context);
        Reminder reminder = databaseHelper.getReminder(id);
        databaseHelper.close();

        mvpView.showValues(reminder);
        mvpView.showSucces(true);
    }

    @Override
    public String formatText(int index) {
        String format;
        String[] array = context.getResources().getStringArray(R.array.repeat);
        switch (index) {
            case Reminder.DOES_NOT_REPEAT:
            default:
                format = array[index];
                break;
            case Reminder.HOURLY:
                format = array[index];
                break;
            case Reminder.DAILY:
                format = array[index];
                break;
            case Reminder.WEEKLY:
                format = array[index];
                break;
            case Reminder.MONTHLY:
                format = array[index];
                break;
            case Reminder.YEARLY:
                format = array[index];
                break;
        }
        return format;
    }
}
