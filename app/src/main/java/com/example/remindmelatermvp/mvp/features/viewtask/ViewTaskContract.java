package com.example.remindmelatermvp.mvp.features.viewtask;

import android.content.Context;

import com.example.remindmelatermvp.mvp.entities.Reminder;

public interface ViewTaskContract {
    public interface View {
        void showValues(Reminder reminder);

        Context getContext();

        void showSucces(boolean isSuccess);
    }

    public interface Presenter {

        void deleteReminder(int id);

        void getReminder(int id);

        String formatText(int index);
    }
}
