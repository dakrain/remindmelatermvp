package com.example.remindmelatermvp.mvp.features.mainpage;

import android.content.Context;
import android.util.Log;

import com.example.remindmelatermvp.database.DatabaseHelper;
import com.example.remindmelatermvp.mvp.entities.Reminder;

import java.util.List;

public class MainTaskPresenter implements MainTaskContract.Presenter {
    private MainTaskContract.View mvpView;
    private DatabaseHelper databaseHelper;
    private Context context;

    public MainTaskPresenter(MainTaskContract.View mvpView, Context context) {
        this.mvpView = mvpView;
        this.context = context;
    }

    @Override
    public void getAllReminder(int isActive) {
        List<Reminder> list;
        databaseHelper = DatabaseHelper.getInstance(context);
        list = databaseHelper.getAllReminder(isActive);
        mvpView.showAllReminder(list);
    }
}
