package com.example.remindmelatermvp.mvp.features.mainpage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.remindmelatermvp.R;
import com.example.remindmelatermvp.mvp.entities.Reminder;
import com.example.remindmelatermvp.utils.DateAndTimeUtil;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainTaskAdapter extends RecyclerView.Adapter<MainTaskAdapter.ViewHolder> {

    private Context context;
    private List<Reminder> reminderList;
    private OnItemClicked onItemClicked;

    public MainTaskAdapter(Context context, List<Reminder> reminderList, OnItemClicked onItemClicked) {
        this.onItemClicked = onItemClicked;
        this.reminderList = reminderList;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_task_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final Reminder reminder = reminderList.get(position);
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        Date date1 = new Date(reminder.getDateAndTime());
        cal1.setTime(date1);
        if (position > 0) {
            Date date2 = new Date(reminderList.get(position - 1).getDateAndTime());
            cal2.setTime(date2);
        }

        if (position > 0 && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR)) {

            viewHolder.textSeparator.setVisibility(View.GONE);
        } else {
            String appropriateDate = DateAndTimeUtil.getAppropriateDateFormat(context, cal1);
            viewHolder.textSeparator.setText(appropriateDate);
            viewHolder.textSeparator.setVisibility(View.VISIBLE);
        }
        viewHolder.setOnItemClicked(new OnItemClicked() {
            @Override
            public void onItemClicked(int id) {
                onItemClicked.onItemClicked(reminder.getId());
            }
        });
        viewHolder.title.setText(reminder.getTitle());
        viewHolder.content.setText(reminder.getDescription());
        viewHolder.time.setText(DateAndTimeUtil.parseTime(reminder.getDateAndTime()));
    }

    @Override
    public int getItemCount() {
        return reminderList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.reminder_title)
        TextView title;
        @BindView(R.id.reminder_time)
        TextView time;
        @BindView(R.id.reminder_description)
        TextView content;
        @BindView(R.id.header_separator)
        TextView textSeparator;
        private View view;
        OnItemClicked onItemClicked;

        public ViewHolder(@NonNull View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        public void setOnItemClicked(OnItemClicked onItemClicked) {
            this.onItemClicked = onItemClicked;
        }

        @Override
        public void onClick(View v) {
            onItemClicked.onItemClicked(getAdapterPosition());
        }
    }

    public interface OnItemClicked {
        void onItemClicked(int id);
    }
}
