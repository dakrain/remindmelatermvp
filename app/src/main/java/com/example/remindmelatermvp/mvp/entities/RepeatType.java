package com.example.remindmelatermvp.mvp.entities;

public enum RepeatType {
    NO_REPEAT,
    HOURLY,
    DAILY,
    WEEKLY,
    MONTHLY,
    YEARLY
}
