package com.example.remindmelatermvp.mvp.features.viewtask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.example.remindmelatermvp.R;
import com.example.remindmelatermvp.mvp.entities.Reminder;
import com.example.remindmelatermvp.mvp.features.taskactivity.TaskActivity;
import com.example.remindmelatermvp.receiver.AlarmReceiver;
import com.example.remindmelatermvp.utils.AlarmUtil;
import com.example.remindmelatermvp.utils.DateAndTimeUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewTaskActivity extends AppCompatActivity implements ViewTaskContract.View {
    private static String REMINDER_ID = "REMINDER_ID";

    private ViewTaskPresenter presenter;
    private int id;

    @BindView(R.id.reminder_title)
    TextView mTitle;
    @BindView(R.id.reminder_description)
    TextView mContent;
    @BindView(R.id.tvOccured)
    TextView mOccurred;
    @BindView(R.id.tvRepeat)
    TextView mRepeat;
    @BindView(R.id.tvTime)
    TextView mTime;
    @BindView(R.id.reminder_time)
    TextView mReminderTime;
    @BindView(R.id.tvDate)
    TextView mDate;
    @BindView(R.id.toolBar)
    Toolbar toolbar;

    public static void showMe(Activity activity, int id) {
        Intent intent = new Intent(activity, ViewTaskActivity.class);
        intent.putExtra(REMINDER_ID, id);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        ButterKnife.bind(this);
        setToolBar();
        id = getIntent().getIntExtra(REMINDER_ID, 0);

        presenter = new ViewTaskPresenter(this, this);
        presenter.getReminder(id);
    }

    @Override
    public void showValues(Reminder reminder) {
        String repeat = getResources().getString(R.string.task_occurred) + " " + reminder.getTimeOccurred()
                + " " + getResources().getString(R.string.task_occurred_on) + " " + reminder.getTimeOccur();

        mTitle.setText(reminder.getTitle());
        mContent.setText(reminder.getDescription());
        mTime.setText(DateAndTimeUtil.parseTime(reminder.getDateAndTime()));
        mReminderTime.setText(DateAndTimeUtil.parseTime(reminder.getDateAndTime()));
        mDate.setText(DateAndTimeUtil.parseDate(reminder.getDateAndTime()));
        mRepeat.setText(presenter.formatText(reminder.getRepeatType()));
        mOccurred.setText(repeat);

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter("BROADCAST_REFRESH"));
        presenter.getReminder(id);
    }

    public Context getContext() {
        return this;
    }

    @Override
    public void showSucces(boolean isSuccess) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionDelete:
                showDialog();
                return true;
            case R.id.actionEdit:
                TaskActivity.showMe(this, id);
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setToolBar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        if (getActionBar() != null) getActionBar().setDisplayHomeAsUpEnabled(true);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle(null);
    }

    public void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.delete_dialog_title));
        builder.setMessage(getString(R.string.delete_dialog_content));
        builder.setNegativeButton(getString(R.string.delete_dialog_negative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setPositiveButton(getString(R.string.delete_dialog_positive), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.deleteReminder(id);
                Intent intent = new Intent(ViewTaskActivity.this, AlarmReceiver.class);
                AlarmUtil.cancelAlarm(ViewTaskActivity.this, intent, id);
                finish();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            presenter.getReminder(id);
        }
    };

}
