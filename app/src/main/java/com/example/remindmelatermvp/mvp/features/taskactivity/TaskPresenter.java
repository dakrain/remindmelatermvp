package com.example.remindmelatermvp.mvp.features.taskactivity;

import com.example.remindmelatermvp.database.DatabaseHelper;
import com.example.remindmelatermvp.mvp.entities.Reminder;
import com.example.remindmelatermvp.utils.DateAndTimeUtil;

import java.util.Date;

public class TaskPresenter implements TaskContract.Presenter {
    private TaskContract.View mvpView;
    private DatabaseHelper reminderDatabase;

    public TaskPresenter(TaskContract.View mvpView) {
        this.mvpView = mvpView;
        reminderDatabase = DatabaseHelper.getInstance(mvpView.getContext());
        reminderDatabase.close();
    }

    @Override
    public void addReminder(Reminder reminder) {
        reminderDatabase.addReminder(reminder);
        reminderDatabase.close();
        mvpView.showSucces(true);
    }

    @Override
    public void caculatedDate(long mDate) {
        Date date = new Date(mDate);
        String stringDate = DateAndTimeUtil.parseDate(date.getTime());
        mvpView.showDate(stringDate);

    }

    @Override
    public void caculatedTime(long time) {
        Date date = new Date(time);
        String stringTime = DateAndTimeUtil.parseTime(date.getTime());
        mvpView.showTime(stringTime);
    }


    @Override
    public void getReminder(int id) {
        Reminder reminder = reminderDatabase.getReminder(id);
        reminderDatabase.close();
        mvpView.showEditReminder(reminder);
    }

    @Override
    public void initView() {
    }
}
