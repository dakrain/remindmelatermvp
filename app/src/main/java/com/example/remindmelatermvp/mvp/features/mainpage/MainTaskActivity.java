package com.example.remindmelatermvp.mvp.features.mainpage;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.remindmelatermvp.R;
import com.example.remindmelatermvp.mvp.entities.Reminder;
import com.example.remindmelatermvp.mvp.features.taskactivity.TaskActivity;
import com.example.remindmelatermvp.mvp.features.viewtask.ViewTaskActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainTaskActivity extends AppCompatActivity implements MainTaskContract.View, MainTaskAdapter.OnItemClicked {
    private MainTaskPresenter presenter;
    private MainTaskAdapter adapter;
    private int isActive = 1;

    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.toolBar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;


    public static void showMe(Activity activity) {
        Intent intent = new Intent(activity, MainTaskActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        ButterKnife.bind(this);
        presenter = new MainTaskPresenter(this, this);
        initView();
        presenter.getAllReminder(isActive);

    }

    @Override
    public void showAllReminder(List<Reminder> list) {
        if (list.size() != 0) {
            emptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter = new MainTaskAdapter(this, list, this);
            recyclerView.setAdapter(adapter);
        } else {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter("BROADCAST_REFRESH"));
        presenter.getAllReminder(isActive);
    }

    @OnClick(R.id.addReminder)
    public void addReminder() {
        TaskActivity.showMe(this, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                showSubMenu();
                break;
        }
        return true;
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            presenter.getAllReminder(isActive);
        }
    };

    private void initView() {
        LinearLayoutManager linearLayout = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayout);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(null);
        }
    }

    @Override
    public void onItemClicked(int id) {
        ViewTaskActivity.showMe(this, id);
    }

    public void showSubMenu() {
        PopupMenu menu = new PopupMenu(this, findViewById(R.id.action_filter));
        menu.getMenuInflater().inflate(R.menu.menu_filter, menu.getMenu());

        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.active:
                        isActive = 1;
                        presenter.getAllReminder(isActive);
                        break;
                    case R.id.completed:
                        isActive = 0;
                        presenter.getAllReminder(isActive);
                        break;
                }
                return true;
            }
        });
        menu.show();
    }
}



