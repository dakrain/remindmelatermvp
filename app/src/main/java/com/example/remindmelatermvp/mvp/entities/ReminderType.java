package com.example.remindmelatermvp.mvp.entities;

public enum ReminderType {
    NOTE,
    REMINDER,
    SMSREMINDER,
    CALLREMINDER,
    EMAILREMINDER
}
