package com.example.remindmelatermvp.mvp.features.taskactivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.remindmelatermvp.R;
import com.example.remindmelatermvp.database.DatabaseHelper;
import com.example.remindmelatermvp.mvp.entities.Reminder;
import com.example.remindmelatermvp.receiver.AlarmReceiver;
import com.example.remindmelatermvp.utils.AlarmUtil;
import com.example.remindmelatermvp.utils.AnimationUtil;
import com.example.remindmelatermvp.utils.DateAndTimeUtil;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TaskActivity extends AppCompatActivity implements TaskContract.View {
    private static String REMINDER_ID = "REMINDER_ID";
    private static int radioState;
    private TaskPresenter presenter;
    private Calendar calendar;
    private String isForever;
    private int id, mRepeatType, mTimeOccur = 1, mTimeOccurred = 0, mInterval = 1;

    @BindView(R.id.edtTitle)
    EditText mTitle;
    @BindView(R.id.edtContent)
    EditText mContent;
    @BindView(R.id.tvTime)
    TextView mTime;
    @BindView(R.id.tvDate)
    TextView mDate;
    @BindView(R.id.tvTimeOccur)
    TextView mOccur;
    @BindView(R.id.rowTime)
    LinearLayout rowTime;
    @BindView(R.id.rowDate)
    LinearLayout rowDate;
    @BindView(R.id.rowInterval)
    LinearLayout rowInterval;
    @BindView(R.id.toolBar)
    Toolbar toolbar;
    @BindView(R.id.tvRepeat)
    TextView mRepeat;


    public static void showMe(Activity activity, int id) {
        Intent intent = new Intent(activity, TaskActivity.class);
        intent.putExtra(REMINDER_ID, id);
        activity.startActivity(intent);

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_edit_task);
        ButterKnife.bind(this);
        calendar = Calendar.getInstance();
        presenter = new TaskPresenter(this);
        initView();
        setToolBar();
    }

    private void initView() {
        Intent intent = getIntent();
        id = intent.getIntExtra(REMINDER_ID, 0);
        if (id == 0) {
            id = DatabaseHelper.getInstance(this).getLastID() + 1;
        } else {
            presenter.getReminder(id);
        }

    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showSucces(boolean isSuccess) {
        if (isSuccess) {
            Toast.makeText(this, "SUCCESS", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showEditReminder(Reminder reminder) {
        String[] array = getResources().getStringArray(R.array.repeat);

        Date date = new Date(reminder.getDateAndTime());
        calendar.setTime(date);
        mTitle.setText(reminder.getTitle());
        mContent.setText(reminder.getDescription());
        mTime.setText(DateAndTimeUtil.parseTime(reminder.getDateAndTime()));
        mDate.setText(DateAndTimeUtil.parseDate(reminder.getDateAndTime()));
        mTimeOccur = reminder.getTimeOccur();
        mTimeOccurred = reminder.getTimeOccurred();
        mRepeatType = reminder.getRepeatType();

        mRepeat.setText(array[reminder.getRepeatType()]);
        mOccur.setText(String.valueOf(reminder.getTimeOccur()) + " " + getResources().getString(R.string.occur_string));
        radioState = 0;
        if (Boolean.parseBoolean(reminder.getIsForever())) {
            radioState = 1;
            mOccur.setText(getResources().getString(R.string.forever_string));
        }

    }

    @Override
    public void showTime(String time) {
        mTime.setText(time);
    }

    @Override
    public void showDate(String date) {
        mDate.setText(date);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_save:
                validateInput();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setToolBar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        if (getActionBar() != null) getActionBar().setDisplayHomeAsUpEnabled(true);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle(null);
    }

    @OnClick(R.id.rowTime)
    public void timePicker() {
        new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                calendar.set(Calendar.MILLISECOND, 0);
                presenter.caculatedTime(calendar.getTimeInMillis());
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(this))
                .show();
    }

    @OnClick(R.id.rowDate)
    public void datePicker(View view) {
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                presenter.caculatedDate(calendar.getTimeInMillis());
            }
        }, calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DATE)).show();
    }

    @OnClick(R.id.rowRepeat)
    public void repeatPicker() {
        final String[] repeatItems = getResources().getStringArray(R.array.repeat);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(repeatItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mRepeatType = which;
                if (which == 0) {
                    rowInterval.setVisibility(View.GONE);
                } else {
                    rowInterval.setVisibility(View.VISIBLE);
                    intervalPicker();
                }
                mRepeat.setText(repeatItems[which]);
            }
        });
        builder.show();
    }

    @OnClick(R.id.rowInterval)
    public void intervalPicker() {

        final View view = View.inflate(this, R.layout.dialog_interval_picker, null);

        final RadioButton buttonTimeOccur = view.findViewById(R.id.radioButton2);
        final RadioButton buttonForever = view.findViewById(R.id.radioButton);
        final EditText timeOccurred = view.findViewById(R.id.timeOccurred);


        //Nếu như text được focus thì set button interval
        timeOccurred.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    buttonTimeOccur.setChecked(true);
                }
            }
        });


        if (radioState == 1) {
            buttonForever.setChecked(true);
        } else {
            buttonTimeOccur.setChecked(true);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Please select Repeat method.");
        builder.setView(view);

        //Assign value of Picker
        timeOccurred.setText(String.valueOf(mInterval));

        builder.setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (buttonTimeOccur.isChecked()) {
                    radioState = 0;
                    mTimeOccur = Integer.parseInt(timeOccurred.getText().toString());
                    mOccur.setText(mTimeOccur + " " + getResources().getString(R.string.occur_string));
                } else {
                    radioState = 1;
                    isForever = Boolean.toString(buttonForever.isChecked());
                    mOccur.setText(R.string.forever_string);
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        ButterKnife.bind(alertDialog);
        builder.show();
    }

    public void validateInput() {
        Calendar nowCalendar = Calendar.getInstance();


        if (mTime.getText().toString().equals(getString(R.string.task_time))) {
            calendar.set(Calendar.HOUR_OF_DAY, nowCalendar.get(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE, nowCalendar.get(Calendar.MINUTE));
        }

        if (mDate.getText().toString().equals(getString(R.string.task_date))) {
            calendar.set(Calendar.YEAR, nowCalendar.get(Calendar.YEAR));
            calendar.set(Calendar.MONTH, nowCalendar.get(Calendar.MONTH));
            calendar.set(Calendar.DAY_OF_MONTH, nowCalendar.get(Calendar.DAY_OF_MONTH));
        }


        if (TextUtils.isEmpty(mTitle.getText().toString())) {
            AnimationUtil.shakeView(mTitle, this);
            Toast.makeText(this, "TEST", Toast.LENGTH_SHORT).show();
        }
        if (DateAndTimeUtil.toLongDateAndTime(calendar) >= DateAndTimeUtil.toLongDateAndTime(nowCalendar)) {
            Reminder reminder = new Reminder(id, mTitle.getText().toString(), mContent.getText().toString(), 1, calendar.getTimeInMillis(), mRepeatType, mInterval, mTimeOccur, mTimeOccurred, isForever);
            presenter.addReminder(reminder);

            Intent alarmIntent = new Intent(this, AlarmReceiver.class);
            calendar.set(Calendar.SECOND, 0);

            AlarmUtil.setAlarm(this, alarmIntent, reminder.getId(), calendar);

            Date date = new Date(reminder.getDateAndTime());
            calendar.setTime(date);

            finish();
        } else {
            Toast.makeText(this, "Cant not initial date in the past", Toast.LENGTH_SHORT).show();
        }
    }
}
