package com.example.remindmelatermvp.mvp.features.mainpage;

import android.content.Context;

import com.example.remindmelatermvp.mvp.entities.Reminder;

import java.util.List;

public interface MainTaskContract {
    public interface View {
        void showAllReminder(List<Reminder> list);

    }

    public interface Presenter {
        void getAllReminder(int isActive);
    }
}
